From c998b285e4d3c75e072be833b4fb9e1b4263f7bf Mon Sep 17 00:00:00 2001
From: Andrea Cervesato <andrea.cervesato@suse.com>
Date: Mon, 4 Nov 2024 15:36:37 +0100
Subject: [PATCH 4/4] Add error coverage for landlock network support

Add two more errors checks inside the landlock02 which is testing
landlock_add_rule syscall. In particular, test now verifies when the
syscall is raising EINVAL due to invalid network attributes.

Reviewed-by: Cyril Hrubis <chrubis@suse.cz>
Signed-off-by: Andrea Cervesato <andrea.cervesato@suse.com>
---
 .../kernel/syscalls/landlock/landlock02.c     | 93 ++++++++++++++-----
 1 file changed, 72 insertions(+), 21 deletions(-)

diff --git a/testcases/kernel/syscalls/landlock/landlock02.c b/testcases/kernel/syscalls/landlock/landlock02.c
index 8566d407f..e1e3228b5 100644
--- a/testcases/kernel/syscalls/landlock/landlock02.c
+++ b/testcases/kernel/syscalls/landlock/landlock02.c
@@ -20,93 +20,142 @@
 
 #include "landlock_common.h"
 
-static struct tst_landlock_ruleset_attr_abi1 *ruleset_attr;
+static struct tst_landlock_ruleset_attr_abi1 *attr_abi1;
+static struct tst_landlock_ruleset_attr_abi4 *attr_abi4;
 static struct landlock_path_beneath_attr *path_beneath_attr;
 static struct landlock_path_beneath_attr *rule_null;
+static struct landlock_net_port_attr *net_port_attr;
 static int ruleset_fd;
 static int invalid_fd = -1;
+static int abi_current;
 
 static struct tcase {
 	int *fd;
-	enum landlock_rule_type rule_type;
-	struct landlock_path_beneath_attr **attr;
+	int rule_type;
+	struct landlock_path_beneath_attr **path_attr;
+	struct landlock_net_port_attr **net_attr;
 	int access;
 	int parent_fd;
+	int net_port;
 	uint32_t flags;
 	int exp_errno;
+	int abi_ver;
 	char *msg;
 } tcases[] = {
 	{
 		.fd = &ruleset_fd,
-		.attr = &path_beneath_attr,
+		.path_attr = &path_beneath_attr,
 		.access = LANDLOCK_ACCESS_FS_EXECUTE,
 		.flags = 1,
 		.exp_errno = EINVAL,
+		.abi_ver = 1,
 		.msg = "Invalid flags"
 	},
 	{
 		.fd = &ruleset_fd,
-		.attr = &path_beneath_attr,
+		.path_attr = &path_beneath_attr,
 		.access = LANDLOCK_ACCESS_FS_EXECUTE,
 		.exp_errno = EINVAL,
+		.abi_ver = 1,
 		.msg = "Invalid rule type"
 	},
 	{
 		.fd = &ruleset_fd,
 		.rule_type = LANDLOCK_RULE_PATH_BENEATH,
-		.attr = &path_beneath_attr,
+		.path_attr = &path_beneath_attr,
 		.exp_errno = ENOMSG,
+		.abi_ver = 1,
 		.msg = "Empty accesses"
 	},
 	{
 		.fd = &invalid_fd,
-		.attr = &path_beneath_attr,
+		.path_attr = &path_beneath_attr,
 		.access = LANDLOCK_ACCESS_FS_EXECUTE,
 		.exp_errno = EBADF,
+		.abi_ver = 1,
 		.msg = "Invalid file descriptor"
 	},
 	{
 		.fd = &ruleset_fd,
 		.rule_type = LANDLOCK_RULE_PATH_BENEATH,
-		.attr = &path_beneath_attr,
+		.path_attr = &path_beneath_attr,
 		.access = LANDLOCK_ACCESS_FS_EXECUTE,
 		.parent_fd = -1,
 		.exp_errno = EBADF,
+		.abi_ver = 1,
 		.msg = "Invalid parent fd"
 	},
 	{
 		.fd = &ruleset_fd,
 		.rule_type = LANDLOCK_RULE_PATH_BENEATH,
-		.attr = &rule_null,
+		.path_attr = &rule_null,
 		.exp_errno = EFAULT,
+		.abi_ver = 1,
 		.msg = "Invalid rule attr"
 	},
+	{
+		.fd = &ruleset_fd,
+		.rule_type = LANDLOCK_RULE_NET_PORT,
+		.net_attr = &net_port_attr,
+		.access = LANDLOCK_ACCESS_FS_EXECUTE,
+		.net_port = 448,
+		.exp_errno = EINVAL,
+		.abi_ver = 4,
+		.msg = "Invalid access rule for network type"
+	},
+	{
+		.fd = &ruleset_fd,
+		.rule_type = LANDLOCK_RULE_NET_PORT,
+		.net_attr = &net_port_attr,
+		.access = LANDLOCK_ACCESS_NET_BIND_TCP,
+		.net_port = INT16_MAX + 1,
+		.exp_errno = EINVAL,
+		.abi_ver = 4,
+		.msg = "Socket port greater than 65535"
+	},
 };
 
 static void run(unsigned int n)
 {
 	struct tcase *tc = &tcases[n];
+	void *attr = NULL;
 
-	if (*tc->attr) {
-		(*tc->attr)->allowed_access = tc->access;
-		(*tc->attr)->parent_fd = tc->parent_fd;
+	if (tc->abi_ver > abi_current) {
+		tst_res(TCONF, "Minimum ABI required: %d", tc->abi_ver);
+		return;
+	}
+
+	if (tc->path_attr && *tc->path_attr) {
+		(*tc->path_attr)->allowed_access = tc->access;
+		(*tc->path_attr)->parent_fd = tc->parent_fd;
+
+		attr = *tc->path_attr;
+	} else if (tc->net_attr && *tc->net_attr) {
+		(*tc->net_attr)->allowed_access = tc->access;
+		(*tc->net_attr)->port = tc->net_port;
+
+		attr = *tc->net_attr;
 	}
 
 	TST_EXP_FAIL(tst_syscall(__NR_landlock_add_rule,
-		*tc->fd, tc->rule_type, *tc->attr, tc->flags),
-		tc->exp_errno,
-		"%s",
-		tc->msg);
+		*tc->fd, tc->rule_type, attr, tc->flags),
+		tc->exp_errno, "%s", tc->msg);
 }
 
 static void setup(void)
 {
-	verify_landlock_is_enabled();
+	abi_current = verify_landlock_is_enabled();
 
-	ruleset_attr->handled_access_fs = LANDLOCK_ACCESS_FS_EXECUTE;
+	attr_abi1->handled_access_fs =
+		attr_abi4->handled_access_fs = LANDLOCK_ACCESS_FS_EXECUTE;
 
-	ruleset_fd = TST_EXP_FD_SILENT(tst_syscall(__NR_landlock_create_ruleset,
-		ruleset_attr, sizeof(struct tst_landlock_ruleset_attr_abi1), 0));
+	if (abi_current < 4) {
+		ruleset_fd = TST_EXP_FD_SILENT(tst_syscall(__NR_landlock_create_ruleset,
+			attr_abi1, sizeof(struct tst_landlock_ruleset_attr_abi1), 0));
+	} else {
+		ruleset_fd = TST_EXP_FD_SILENT(tst_syscall(__NR_landlock_create_ruleset,
+			attr_abi4, sizeof(struct tst_landlock_ruleset_attr_abi4), 0));
+	}
 }
 
 static void cleanup(void)
@@ -122,8 +171,10 @@ static struct tst_test test = {
 	.cleanup = cleanup,
 	.needs_root = 1,
 	.bufs = (struct tst_buffers []) {
-		{&ruleset_attr, .size = sizeof(struct tst_landlock_ruleset_attr_abi1)},
+		{&attr_abi1, .size = sizeof(struct tst_landlock_ruleset_attr_abi1)},
+		{&attr_abi4, .size = sizeof(struct tst_landlock_ruleset_attr_abi4)},
 		{&path_beneath_attr, .size = sizeof(struct landlock_path_beneath_attr)},
+		{&net_port_attr, .size = sizeof(struct landlock_net_port_attr)},
 		{},
 	},
 	.caps = (struct tst_cap []) {
-- 
2.47.0

